= Programació en bases de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat funcions SQL simples

[NOTE]
====
Per a cada exercici es vol:

- el codi que crea la funció.
- un exemple de com s'utilitza.
- el resultat obtingut.
====

=== Base de dades `world`

1. Fes una funció que rebi el nom d'un país i retorni quantes ciutats del país
hi ha.

2. Fes una funció que rebi el nom d'un idioma i retorni a quants països es parla
aquest idioma.

3. Fes una funció que permeti inserir noves ciutats a la base de dades. La
funció rebrà el nom de la ciutat, el codi del país, el districte i la quantitat
d'habitants. Ha de retornar l'id de la ciutat creada (utilitza `returning`).

4. Fes una funció que permeti modificar la població d'una ciutat. Per fer-ho no
li donarem la població total, sinó quants habitants nous hi ha i quants
habitants han marxat (per exemple, hi ha 300 habitants nous deguts a naixements
i immigració, i 180 habitants menys deguts a defuncions i emigració). La funció
ha de retornar la quantitat d'habitants actualitzada.

=== Base de dades `hotel`

[start=5]
5. Fes una funció que rebi una data i retorni la quantitat d'hostes que hi
havia a l'hotel en aquesta data.

6. Fes una funció que rebi un tipus i número de document i ens retorni a quina
habitació està l'hoste que té aquest document (o NULL si en aquests moments
no es troba a l'hotel).

7. Fes una funció que rebi l'id d'un tipus d'habitació, una data d'entrada i
una data de sortida, i ens retorni un número d'habitació que estigui lliure
del tipus demanat en aquestes dates. Si n'hi ha moltes, fes que retorni la que
tingui un número més baix. Si no hi ha cap habitació disponible, la funció ha
de retornar NULL.

8. Fes una funció que rebi el nom d'una temporada, el nom d'un tipus d'habitació
i un preu, i assigni el preu que rep com a preu del tipus d'habitació
especificat a la temporada especificada. La funció retornarà el preu assignat.

9. Fes una funció que rebi el nom d'un tipus d'habitació i el nom d'una
característica, i retorni si el tipus d'habitació té o no la característica
en qüestió.

10. Fes una funció que rebi un tipus i número de document i ens retorni la
quantitat de nits que l'hoste amb aquest document ha passat a l'hotel. Ha de
retornar 0 si l'hoste no existeix a la base de dades.
