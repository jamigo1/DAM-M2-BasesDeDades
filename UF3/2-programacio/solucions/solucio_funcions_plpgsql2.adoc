= Programació en bases de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat funcions PL/pgSQL 2

=== Solució

1. Fes la funció `search_film` que rebi com a paràmetre un text i retorni un
conjunt de pel·lícules. La funció comprovarà si l'argument conté algun símbol %.
Si no en conté n'afegirà un al principi i un al final. Llavors, cercarà totes
les pel·lícules el títol de les quals coincideixi amb el text rebut.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION search_film(search_string text) RETURNS SETOF film AS $$
BEGIN
  IF position('%' IN search_string) = 0 THEN
    search_string = '%'||search_string||'%';
  END IF;
  RETURN QUERY SELECT * FROM film WHERE title LIKE search_string;
END;
$$ LANGUAGE plpgsql;

select * from search_film('ACAD%');
film_id |      title       |                                           description                                            | release_year | language_id | original_language_id | rental_duration | rental_rate | length | replacement_cost | rating |        last_update         |            special_features            |                                                                  fulltext
---------+------------------+--------------------------------------------------------------------------------------------------+--------------+-------------+----------------------+-----------------+-------------+--------+------------------+--------+----------------------------+----------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------
      1 | ACADEMY DINOSAUR | A Epic Drama of a Feminist And a Mad Scientist who must Battle a Teacher in The Canadian Rockies |         2006 |           1 |                      |               6 |        0.99 |     86 |            20.99 | PG     | 2007-09-10 17:46:03.905795 | {"Deleted Scenes","Behind the Scenes"} | 'academi':1 'battl':15 'canadian':20 'dinosaur':2 'drama':5 'epic':4 'feminist':8 'mad':11 'must':14 'rocki':21 'scientist':12 'teacher':17
(1 row)
----

2. Fes una versió millorada de la funció anterior, `search_film`, que rebi
dos textos. El segon argument podrà ser "title", "description" o "genre". La
cerca es farà com a l'exercici anterior, però pel camp especificat. Si el
camp és incorrecte, cal llançar una excepció.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION search_film(search_string text, search_field text)
      RETURNS SETOF film AS $$
BEGIN
  IF position('%' IN search_string) = 0 THEN
    search_string = '%'||search_string||'%';
  END IF;
  CASE search_field
    WHEN 'title' THEN
      RETURN QUERY SELECT * FROM film WHERE title LIKE search_string;
    WHEN 'description' THEN
      RETURN QUERY SELECT * FROM film WHERE description LIKE search_string;
    WHEN 'genre' THEN
      RETURN QUERY SELECT f.* FROM film f
        JOIN film_category fc ON f.film_id=fc.film_id
        JOIN category c ON fc.category_id=c.category_id
        WHERE c.name LIKE search_string;
    ELSE
      RAISE EXCEPTION 'La cerca ha de ser per title, description o genre.';
  END CASE;
END;
$$ LANGUAGE plpgsql;

select * from search_film('ACAD', 'title');
film_id |      title       |                                             description                                              | release_year | language_id | original_language_id | rental_duration | rental_rate | length | replacement_cost | rating |        last_update         |            special_features            |                                                                  fulltext
---------+------------------+------------------------------------------------------------------------------------------------------+--------------+-------------+----------------------+-----------------+-------------+--------+------------------+--------+----------------------------+----------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------
      1 | ACADEMY DINOSAUR | A Epic Drama of a Feminist And a Mad Scientist who must Battle a Teacher in The Canadian Rockies     |         2006 |           1 |                      |               6 |        0.99 |     86 |            20.99 | PG     | 2007-09-10 17:46:03.905795 | {"Deleted Scenes","Behind the Scenes"} | 'academi':1 'battl':15 'canadian':20 'dinosaur':2 'drama':5 'epic':4 'feminist':8 'mad':11 'must':14 'rocki':21 'scientist':12 'teacher':17
...

select * from search_film('MySQL', 'description');
film_id |         title          |                                                     description                                                      | release_year | language_id | original_language_id | rental_duration | rental_rate | length | replacement_cost | rating |        last_update         |                       special_features                       |                                                                                  fulltext
---------+------------------------+----------------------------------------------------------------------------------------------------------------------+--------------+-------------+----------------------+-----------------+-------------+--------+------------------+--------+----------------------------+--------------------------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     11 | ALAMO VIDEOTAPE        | A Boring Epistle of a Butler And a Cat who must Fight a Pastry Chef in A MySQL Convention                            |         2006 |           1 |                      |               6 |        0.99 |    126 |            16.99 | G      | 2007-09-10 17:46:03.905795 | {Commentaries,"Behind the Scenes"}                           | 'alamo':1 'bore':4 'butler':8 'cat':11 'chef':17 'convent':21 'epistl':5 'fight':14 'must':13 'mysql':20 'pastri':16 'videotap':2
...

select * from search_film('Sci-Fi', 'genre');
film_id |        title         |                                                        description                                                        | release_year | language_id | original_language_id | rental_duration | rental_rate | length | replacement_cost | rating |        last_update         |                       special_features                       |                                                                                              fulltext
---------+----------------------+---------------------------------------------------------------------------------------------------------------------------+--------------+-------------+----------------------+-----------------+-------------+--------+------------------+--------+----------------------------+--------------------------------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     26 | ANNIE IDENTITY       | A Amazing Panorama of a Pastry Chef And a Boat who must Escape a Woman in An Abandoned Amusement Park                     |         2006 |           1 |                      |               3 |        0.99 |     86 |            15.99 | G      | 2007-09-10 17:46:03.905795 | {Commentaries,"Deleted Scenes"}                              | 'abandon':20 'amaz':4 'amus':21 'anni':1 'boat':12 'chef':9 'escap':15 'ident':2 'must':14 'panorama':5 'park':22 'pastri':8 'woman':17
...

select * from search_film('Mad', 'desc');
ERROR:  La cerca ha de ser per title, description o genre.
CONTEXT:  PL/pgSQL function search_film(text,text) line 17 at RAISE
----

3. Fes una funció que rebi el títol d'una pel·lícula i un codi de botiga i
retorni els ítems d'inventari corresponents a aquesta pel·lícula que estiguin a
la botiga indicada i que, a més, no estiguin en lloguer en aquests moments.
+
[TIP]
====
Ja hi ha una funció, `inventory_in_stock`, que retorna si un ítem
d'inventari està en estoc o no.
====
+
[source,sql]
----
CREATE OR REPLACE FUNCTION available_items(title text, store_id int)
      RETURNS SETOF inventory AS $$
BEGIN
  RETURN QUERY SELECT i.* FROM inventory i
    JOIN film f ON i.film_id=f.film_id
    WHERE i.store_id=available_items.store_id
    AND f.title = available_items.title
    AND inventory_in_stock(i.inventory_id) = true;
END;
$$ LANGUAGE plpgsql;

select * from available_items('ACADEMY DINOSAUR', 2);
 inventory_id | film_id | store_id |     last_update
--------------+---------+----------+---------------------
            5 |       1 |        2 | 2006-02-15 10:09:17
            7 |       1 |        2 | 2006-02-15 10:09:17
            8 |       1 |        2 | 2006-02-15 10:09:17
(3 rows)
----

4. Crea una funció que mostri el nom complet, el telèfon i el títol de la
pel·lícula de tots aquells clients que tenen un títol que ja haurien d'haver
retornat. La funció retornarà un conjunt amb tots aquests clients.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION pending_returns() RETURNS SETOF customer AS $$
DECLARE
  c_id integer;
  firstname varchar(45);
  lastname varchar(45);
  phone varchar(20);
  title varchar(255);
BEGIN
  FOR c_id, firstname, lastname, phone, title IN
    SELECT c.customer_id, c.first_name, c.last_name, a.phone, f.title
    FROM customer c
    JOIN rental r ON r.customer_id = c.customer_id
    JOIN inventory i ON i.inventory_id = r.inventory_id
    JOIN film f ON f.film_id = i.film_id
    JOIN address a ON a.address_id = c.address_id
    WHERE r.return_date IS NULL
    AND r.rental_date + f.rental_duration*'1 day'::interval < NOW()
  LOOP
    RAISE NOTICE 'Nom: % %, telèfon: %, títol: %', firstname,
      lastname, phone, title;
    RETURN QUERY SELECT * FROM customer WHERE customer_id = c_id;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

select * from pending_returns();
customer_id | store_id | first_name |  last_name  |                 email                 | address_id | activebool | create_date |     last_update     | active
-------------+----------+------------+-------------+---------------------------------------+------------+------------+-------------+---------------------+--------
        366 |        1 | BRANDON    | HUEY        | BRANDON.HUEY@sakilacustomer.org       |        371 | t          | 2006-02-14  | 2006-02-15 09:57:20 |      1
        111 |        1 | CARMEN     | OWENS       | CARMEN.OWENS@sakilacustomer.org       |        115 | t          | 2006-02-14  | 2006-02-15 09:57:20 |      1
...
NOTICE:  Nom: BRANDON HUEY, telèfon: 99883471275, títol: ACE GOLDFINGER
NOTICE:  Nom: CARMEN OWENS, telèfon: 272234298332, títol: AFFAIR PREJUDICE
----

5. Fes una funció que permeti retornar una pel·lícula. La funció rebrà
l'identificador del client i l'identificador de l'ítem a retornar. Cal que es
llanci una excepció en cas que el client no tingui en lloguer l'ítem
especificat. El text de l'excepció ha d'indicar si l'ítem no està llogat o, si
ho està, ha d'indicar quin client el té en lloguer.
+
[source,sql]
----
CREATE OR REPLACE FUNCTION return_item(cust_id int, inv_id int) RETURNS void AS $$
DECLARE
  n_rentals bigint;
  actual_cust_id int;
  actual_rental_id int;
BEGIN
  SELECT c.customer_id, r.rental_id INTO actual_cust_id, actual_rental_id
    FROM customer c
    JOIN rental r ON c.customer_id=r.customer_id
    WHERE r.inventory_id=inv_id AND return_date IS NULL;
  IF actual_cust_id IS NULL THEN -- No està llogat
    RAISE EXCEPTION 'L''ítem % no està llogat.', inv_id;
  ELSIF actual_cust_id!=cust_id THEN -- Llogat per un altre client
    RAISE EXCEPTION 'L''ítem % està llogat pel client %.', inv_id, actual_cust_id;
  ELSE -- Finalitzem el lloguer
    UPDATE rental SET return_date = NOW() WHERE rental_id=actual_rental_id;
    RAISE NOTICE 'S''ha actualitzat el lloguer número %.', actual_rental_id;
  END IF;
END;
$$ LANGUAGE plpgsql;

select return_item(1, 4);
ERROR:  L'ítem 4 no està llogat.
CONTEXT:  PL/pgSQL function return_item(integer,integer) line 12 at RAISE

select return_item(1, 5);
ERROR:  L'ítem 5 no està llogat.
CONTEXT:  PL/pgSQL function return_item(integer,integer) line 12 at RAISE

select return_item(1, 6);
ERROR:  L'ítem 6 està llogat pel client 554.
CONTEXT:  PL/pgSQL function return_item(integer,integer) line 14 at RAISE

select return_item(554, 6);
NOTICE:  S'ha actualitzat el lloguer número 14098.
 return_item
-------------

(1 row)
----
