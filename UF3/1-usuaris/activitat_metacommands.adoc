= Llenguatges SQL: DCL
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Metaordres psql

Utilitzant els apunts i la
link:https://www.postgresql.org/docs/11/app-psql.html[documentació oficial del psql]
troba, sobre la base de dades `pagila`:

1. Quins esquemes té.

2. Quines vistes té.

3. Les sentències SELECT que defineixen les diverses vistes.

4. Quines funcions d'usuari hi ha definides.

5. El codi font de les funcions d'usuari.
