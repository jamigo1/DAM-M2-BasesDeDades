= MongoDB
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat consultes 1

Utilitzarem la col·lecció link:http://media.mongodb.org/zips.json[zips].

Per importar la base de dades segueix els passos següents:

1. Descarrega el fitxer. Importa'l amb:
+
----
$ mongoimport -d test -c zips <ruta al fitxer zips.json>
----

2. El sistema t'ha d'informar que s'han importat 29353 documents.

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- La consulta demanada.
- Els resultats obtinguts (o part dels resultats si en retorna molts).
====

1. Troba les dades del codi postal 01222. Nota que el codi postal és l'element
que fa d'identificador.

2. Troba el document que s'ha inserit a la posició 2421.

3. Sabent que hem importat 29353 documents, mostra els 5 últims.

4. Troba tots els codis postals de la ciutat de Nova York.

5. Troba tots els codis postals de la ciutat de Nova York que tenen més de
50000 habitants.

6. Troba tots els codis postals que pertanyen a la ciutat de Nova York o que
tenen més de noranta mil habitants.

7. Troba tots els documents que tenen un nom de població començat per 'Z'.

8. Cerca tots els documents de l'estat d'Oregon (OR) i de l'estat de Nova York
(NY). Utilitza `$in`.

9. Sabent que el codi postal més gran de la col·lecció és el 99950, afegeix un
nou document amb la mateixa estructura dels documents existents.

10. Afegeix ara dos documents més, utilitzant una única sentència.
