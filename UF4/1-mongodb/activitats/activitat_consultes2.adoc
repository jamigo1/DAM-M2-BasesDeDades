= MongoDB
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat consultes 2

Utilitzarem la col·lecció link:http://media.mongodb.org/zips.json[zips].

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- La consulta demanada.
- Els resultats obtinguts (o part dels resultats si en retorna molts).
====

1. Troba quants codis postals tenen més de 60000 habitants.

2. Troba el codi postal, el nom de la població i la quantitat d'habitants dels
10 codis postals més poblats d'Ohio (OH).

3. Augmenta en 1000 la població del codi postal que vas afegir a l'activitat
anterior.

4. Esborra el document que vas inserir a l'activitat anterior.

5. Augmenta un 4% la població de tots els codis postals de l'estat de Texas
(TX).

6. Mostra una llista amb tots els estats d'Estats Units, ordenada
alfabèticament.
+
[TIP]
====
Utilitza
link:https://docs.mongodb.com/manual/reference/method/db.collection.distinct/[distinct].
====

7. Troba quantes poblacions diferents hi ha a l'estat de Nova York.
+
[TIP]
====
Recorda que pots utilitzar qualsevol propietat de JavaScript.
====

8. Troba els codis postals i poblacions que estan més a l'est del meridià -74.
+
[TIP]
====
El meridià és la primera coordenada de `loc`. Com més a l'est més gran és el
meridià.
====

9. Troba els tres codis postals i les poblacions corresponents que més aprop
estan del paral·lel 42.
+
[TIP]
====
El paral·lel és la segona coordenada de `loc`.

Utilitza `toArray()` per passar un cursor de MongoDB a un array de JavaScript.

Utilitza el `sort()` de JavaScript per realitzar una ordenació complexa.
====

10. Mostra els codis postals de l'estat de California (CA), ordenats d'oest a
est.

11. Dobla la població dels 8 codis postals més poblats de California (CA).
+
[TIP]
====
Utilitza Javascript per recórrer els documents que retorni la cerca.
====
