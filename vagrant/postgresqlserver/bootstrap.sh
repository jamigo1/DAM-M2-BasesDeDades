#!/bin/sh

# Configuració locales
sed -i s/"# en_US.UTF-8 UTF-8"/"en_US.UTF-8 UTF-8"/ /etc/locale.gen
sed -i s/"# es_ES.UTF-8 UTF-8"/"es_ES.UTF-8 UTF-8"/ /etc/locale.gen
sed -i s/"# ca_ES.UTF-8 UTF-8"/"ca_ES.UTF-8 UTF-8"/ /etc/locale.gen
locale-gen

# Instal·lació postgresql
apt-get update
apt-get install -y postgresql

# Crea usuari admin
su - postgres -c "createuser -d -r -P admin" <<EOF
super3
super3
EOF

# Importa les BD
psql -h 127.0.0.1 -U admin -d postgres <<EOF
super3
\i /db/pagila.sql
\i /db/world.sql
\i /db/chinook.sql
\i /db/hotel.sql
\i /db/miniwind.sql
\i /db/northwind.sql
\i /db/sakila.sql
\i /db/traders.sql
EOF

# Permet connexions des del host
sed -i s/"127.0.0.1\/32"/"0.0.0.0\/0"/ /etc/postgresql/11/main/pg_hba.conf
sed -i s/"^#listen_addresses = 'localhost'"/"listen_addresses = '*'"/ /etc/postgresql/11/main/postgresql.conf
systemctl restart postgresql
