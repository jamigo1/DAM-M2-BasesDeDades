#!/bin/sh

# Instal·lació mongodb
apt-get update
apt-get install -y dirmngr
apt-key adv --keyserver http://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.0 main" | tee /etc/apt/sources.list.d/mongodb-org-4.0.list
apt-get update
apt-get install -y mongodb-org
systemctl enable mongod
systemctl start mongod

# Importa col·lecció books
wget -nv https://gitlab.com/joanq/DAM-M2-BasesDeDades/raw/master/db/mongodb/books.json
mongoimport -d books -c books < books.json

# Importa col·lecció zips
wget -nv https://media.mongodb.org/zips.json
mongoimport -d zips -c zips < zips.json

# Importa col·lecció countries
wget -nv https://github.com/mledoze/countries/raw/master/countries.json
mongoimport -d countries -c countries --jsonArray < countries.json
