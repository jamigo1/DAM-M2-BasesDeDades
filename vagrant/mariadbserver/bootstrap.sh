#!/bin/sh

# Instal·lació mariadb
apt-get update
apt-get install -y mariadb-server

# Importa BD
mysql < /db/sakila_onlytables/sakila-schema.sql
mysql < /db/sakila_onlytables/sakila-data.sql
mysql < /db/miniwind.sql
mysql < /db/Chinook_MySql_AutoIncrementPKs.sql
mysql < /db/Chinook_MySql.sql
mysql < /db/lastnames.sql
mysql < /db/KnightsDB.sql
mysql < /db/traders.sql
mysql < /db/northwind.sql
mysql < /db/northwind-data.sql
mysql < /db/hotel.sql

# Configuració de MariaDB:
# - Permet connexions des de qualsevol host
# - Activa GROUP BY estricte
# - Permet || com a CONCAT (PIPES_AS_CONCAT)
# - No permet " com a delimitador de cadenes, només ' (ANSI_QUOTES)
cp /vagrant/50-server.cnf /etc/mysql/mariadb.conf.d

# Crea l'usuari admin amb accés remot
mysql << EOF
CREATE OR REPLACE USER admin@'%' IDENTIFIED BY 'super3';
GRANT ALL ON *.* TO admin@'%';
EOF
systemctl restart mariadb
