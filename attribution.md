# Atribucions

Aquest material ha estat creat per en Joan Queralt Molina a partir de diverses
fonts.

La part de teoria de les unitats 1 i 2 s'han basat en el llibre
__Database design - 2nd Editon__ de *Adrienne Watt* i *Nelson Eng*, disponible
sota una llicència CC-BY a la web de [BCCampus](https://open.bccampus.ca/).

S'ha partit d'una traducció i reordenació d'aquests materials, i posteriorment
s'han modificat àmpliament algunes parts, se n'han refet algunes altres, i se
n'han afegit de noves.

Per exemple, totes les sentències SQL del llibre esmentat s'executen sobre
MS-SQL, però s'han modificat tots els exemples adaptant-los a MySQL/MariaDB, a
part d'afegir-ne molts de nous. També, l'apartat de transaccions no existia al
llibre, i altres parts, com el tractament del valor null, s'han reescrit
completament.

Els exercicis d'aquestes unitats són propis, excepte els indicats a continuació:

- _El campionat de gimnàstica rítmica_, _el festival de música_ i
_les eleccions_ són de la Isabel de Andrès, reproduïts i adaptats amb permís de
l'autora.

- _El fabricant_ i _el concessionari_ són versions adaptades d'exercicis del
llibre __Database design - 2nd Editon__, citat anteriorment.

Totes les solucions són pròpies.

La teoria de la UF3 s'ha escrit partint de la documentació oficial de
PostgreSQL. Concretament, el
[Comprehensive Manual](https://www.postgresql.org/docs/manuals/), disponible
sota la llicència lliure i permissiva de PostgreSQL, i el
[manual de referència](https://www.postgresql.org/docs/current/static/reference.html),
disponible sota la mateixa llicència.

Totes les activitats i solucions són pròpies, amb les següents excepcions:

- Les activitats _creació d'usuaris_ i _gestió d'usuaris sobre miniwind_ són de
la Gemma Raimat, reproduïts i adaptats amb el permís de l'autora.

El material de MongoDB de la UF4 és propi.

Com que la llicència de la documentació oficial (CC BY-NC-SA 3.0 US) no és
lliure, no s'han pogut utilitzar les seves explicacions i exemples, i s'han
hagut de crear des de zero.

La col·lecció de dades per MongoDB _books.json_ és pròpia. S'ha generat
utilitzant les eines de https://www.mockaroo.com/. La llista de títols de
llibres s'ha creat amb els generadors de http://www.fantasynamegenerators.com/.

La base de dades MyWind (Northwind) s'ha extret de https://github.com/dalers/mywind,
publicada sota la llicència BSD simplificada.

A continuació teniu els termes legals dels materials utilitzats.

## Database design - 2nd Edition

by Adrienne Watt and Nelson Eng

Unless otherwise noted within this book, this book is released under a 
[Creative Commons
Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/) also known as a CC-BY license. This
means you are free to copy, redistribute, modify or adapt this book.
Under this license, anyone who redistributes or modifies this textbook,
in whole or in part, can do so for free providing they properly
attribute the book.

Additionally, if you redistribute this textbook, in whole or in part, in
either a print or digital format, then you must retain on every physical
and/or electronic page the following attribution:

**Download this book for free at http://open.bccampus.ca**

Cover image:
[Spiral Stairs In Milano old building downtown](http://opentextbc.ca/dbdesign01/%28http://www.flickr.com/photos/micurs/3498339723/in/photostream/)
by [Michele Ursino](http://www.flickr.com/photos/micurs/) used under a
[CC-BY-SA 2.0 license](http://creativecommons.org/licenses/by-sa/2.0/deed.en_CA).

Database Design - 2nd Edition by
[Adrienne Watt and Nelson Eng](http://opentextbc.ca/dbdesign01) is licensed
under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/),
except where otherwise noted.

## PostgreSQL 9.6.3 Documentation

by The PostgreSQL Global Development Group

Copyright © 1996-2017 The PostgreSQL Global Development Group

### Legal Notice

PostgreSQL is Copyright © 1996-2017 by the PostgreSQL Global Development Group.

Postgres95 is Copyright © 1994-5 by the Regents of the University of California.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose, without fee, and without a written agreement is
hereby granted, provided that the above copyright notice and this paragraph and
the following two paragraphs appear in all copies.

IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN “AS-IS” BASIS, AND
THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

## Base de dades MyWind

MySQL version of Northwind demo database.

Copyright (c) 2014, Dale Scott
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
